{ obelisk ? import ./.obelisk/impl {
    system = builtins.currentSystem;
    # You must accept the Android Software Development Kit License Agreement at
    # https://developer.android.com/studio/terms in order to build Android apps.
    # Uncomment and set this to `true` to indicate your acceptance:
    # config.android_sdk.accept_license = false;
  }
  , withHoogle ? false
}:
let deps = obelisk.nixpkgs.thunkSet ./dep;
    haskellLib = obelisk.nixpkgs.haskell.lib;
in with obelisk;
project ./. ({ pkgs, hackGet, ... }: {
  packages = {
  };
  overrides = self: super: {
    reflex-gadt-api = pkgs.haskell.lib.doJailbreak (self.callHackage "reflex-gadt-api" "0.2.2.1" {});
    beam-migrate = haskellLib.dontCheck (haskellLib.doJailbreak (self.callHackage "beam-migrate" "0.5.1.2" {}));
    beam-core = haskellLib.doJailbreak (self.callHackage "beam-core" "0.9.2.1" {});
    beam-postgres = haskellLib.dontCheck (haskellLib.doJailbreak (self.callHackage "beam-postgres" "0.5.2.1" {}));
    tmp-postgres = haskellLib.markUnbroken super.tmp-postgres;
    beam-automigrate = haskellLib.dontCheck (self.callHackageDirect {
      pkg = "beam-automigrate";
      ver = "0.1.4.0";
      sha256 = "0q1w7w2q0r6x8ni2daxvcr8karnqf6lzq1bp4x1x9sqprlm8caxb";
    } {});

  };
  android.applicationId = "systems.obsidian.obelisk.examples.lithograph";
  android.displayName = "Lithograph";
  ios.bundleIdentifier = "systems.obsidian.obelisk.examples.lithograph";
  ios.bundleName = "Lithograph";

  inherit withHoogle;
})
