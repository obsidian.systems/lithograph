{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE StandaloneDeriving #-}
module Common.Schema where

import Control.Lens
import Data.Aeson
import Data.ByteString (ByteString)
import Data.Int (Int32)
import Data.Text (Text)
import Data.Time (UTCTime)
import GHC.Generics
import Database.Beam
import Database.Beam.Backend.SQL.Types

data Db f = Db
  { _db_accounts :: f (TableEntity AccountT)
  , _db_posts :: f (TableEntity PostT)
  }
  deriving (Generic, Database be)

type Id = SqlSerial Int32

data AccountT f = Account
  { _account_id :: Columnar f Id
  , _account_email :: Columnar f Text
  , _account_passwordHash :: Columnar f (Maybe ByteString)
  , _account_passwordResetNonce :: Columnar f (Maybe UTCTime)
  }
  deriving (Generic, Beamable)

instance Table AccountT where
  newtype PrimaryKey AccountT f = AccountId { _accountId_id :: Columnar f Id }
  primaryKey = AccountId . _account_id

instance Beamable (PrimaryKey AccountT)
deriving instance Generic (PrimaryKey AccountT f)

type Account = AccountT Identity

data PostT f = Post
  { _post_id :: Columnar f Id
  , _post_title :: Columnar f Text
  , _post_body :: Columnar f Text
  , _post_timestamp :: Columnar f UTCTime
  }
  deriving (Generic, Beamable)

instance Table PostT where
  newtype PrimaryKey PostT f = PostId { _postId_id :: Columnar f Id }
  primaryKey = PostId . _post_id

type PostId = PrimaryKey PostT Identity
type Post = PostT Identity

instance Beamable (PrimaryKey PostT)
deriving instance Generic (PrimaryKey PostT f)
instance Wrapped PostId
deriving instance Show Post

instance ToJSON Post
instance FromJSON Post
instance ToJSON PostId
instance FromJSON PostId
