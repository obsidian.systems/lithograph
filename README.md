# lithograph

This is an example [obelisk](https://github.com/obsidiansystems/obelisk) app that implements a very simple blog.

This app demonstrates:
- Automatic postgres setup via [gargoyle](https://hackage.haskell.org/package/gargoyle)
- Database migrations via [beam-automigrate](https://hackage.haskell.org/package/beam-automigrate)
- Server/client communication via [reflex-gadt-api](https://hackage.haskell.org/package/reflex-gadt-api)
- Server-side rendering (built-in to obelisk)
- Routing (built-in to obelisk)

## Developing

Clone this repository and `ob run`. If you don't have obelisk installed, follow [these instructions](https://github.com/obsidiansystems/obelisk/blob/11beb6e8cd2419b2429925b76a98f24035e40985/README.md#installing-obelisk).

### Creating the first user

```shell
ob repl
:l backend/src-bin/create-account.hs
:main email@email.com password
```