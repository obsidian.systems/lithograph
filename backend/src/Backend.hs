{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
module Backend where

import Control.Monad.IO.Class
import Crypto.PasswordStore (verifyPasswordWith, pbkdf2)
import qualified Data.Aeson as Aeson
import Data.Constraint.Extras
import Data.Pool
import Data.Some
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (encodeUtf8)
import Data.Time (getCurrentTime)
import Database.PostgreSQL.Simple
import Database.Beam.Backend.SQL.BeamExtensions
import Gargoyle.PostgreSQL.Connect (withDb)
import Obelisk.Backend
import Obelisk.Route
import Snap
import System.Directory (createDirectoryIfMissing)
import Web.ClientSession as CS
import Database.Beam
import Database.Beam.Postgres

import Backend.Schema
import Common.Api
import Common.Route
import Common.Schema

encryptToken :: CS.Key -> Account -> IO Token
encryptToken csk acc = do
  tok <- CS.encryptIO csk (encodeUtf8 $ _account_email acc)
  return (Token tok)

decryptToken :: CS.Key -> Token -> Maybe Text
decryptToken csk (Token tok) = do
  dec <- CS.decrypt csk tok
  Aeson.decodeStrict' dec

checkToken :: MonadBeam Postgres m => CS.Key -> Token -> m (Maybe Account)
checkToken csk token = do
  case decryptToken csk token of
    Nothing -> return Nothing
    Just email -> do
      accs <- runSelectReturningList $ select $
        filter_ (\a -> _account_email a ==. val_ email) $
          all_ $ _db_accounts db
      case accs of
        [] -> return Nothing
        [acc] -> return (Just acc)
        _ -> error "checkToken: Account uniqueness constraint violated"

backend :: Backend BackendRoute FrontendRoute
backend = Backend
  { _backend_run = \serve -> do
    -- Retrieve or initialise the client session key, used to encrypt login tokens.
    createDirectoryIfMissing True "config/backend"
    csk <- CS.getKey "config/backend/clientSessionKey"
    -- Initialize the database and connect
    liftIO $ withDb "db" $ \pool -> do
      _ <- withResource pool runMigrations
      -- Handle 'BackendRoute's
      serve $ \case
        -- The Backend_Api route is expected to receive json-encoded API requests
        -- conforming to the 'Api' specification in Common.Api
        BackendRoute_Api :/ () -> do
          b <- readRequestBody 1000000
          case Aeson.eitherDecode b of
            Left err -> do
              modifyResponse $ setResponseStatus 400 "Malformed request"
              writeText $ "Malformed request: " <> T.pack err <> "\n"
            Right (Some reqBody) -> do
              resp <- liftIO $ requestHandler pool csk reqBody
              writeLBS $ has @Aeson.ToJSON @Api reqBody (Aeson.encode resp)
        _ -> return ()
  , _backend_routeEncoder = fullRouteEncoder
  }

requestHandler
  :: Pool Connection
  -> CS.Key
  -> Api resp
  -> IO resp
requestHandler pool csk req = withResource pool $ \conn -> runBeamPostgres conn $ case req of
  Api_Login email password -> do
    accs <- runSelectReturningList $ select $
      filter_ (\a -> _account_email a ==. val_ email) $
        all_ $ _db_accounts db
    case accs of
      [] -> do
        return Nothing
      (acc:_) -> do
        let passwordValid = case _account_passwordHash acc of
              Nothing -> False
              Just ph -> verifyPasswordWith pbkdf2 (2^)
                (encodeUtf8 password) ph
        if passwordValid
          then do
            token <- liftIO $ encryptToken csk acc
            return (Just (email, token))
          else return Nothing
  Api_CreatePost token title body -> do
    _ <- checkToken csk token -- Check that the user's account exists
    t <- liftIO $ getCurrentTime -- TODO get time from db
    ks <- runInsertReturningList $ insert (_db_posts db) $ insertExpressions
      [ Post default_ (val_ title) (val_ body) (val_ t)
      ]
    return $ case ks of
      [k] -> Right $ primaryKey k
      [] -> Left "Post not inserted"
      _ -> Left "Something went wrong"
  Api_GetPosts ->
    runSelectReturningList $ select $
      limit_ 10 $ orderBy_ (desc_ . _post_timestamp) $
        all_ $ _db_posts db
  Api_GetPost pid -> do
    posts <- runSelectReturningList $ select $ limit_ 1 $ filter_ (\p -> _post_id p ==. val_ (_postId_id pid)) $ all_ $ _db_posts db
    return $ case posts of
      [p] -> Just p
      _ -> Nothing
