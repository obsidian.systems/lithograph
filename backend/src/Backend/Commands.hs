module Backend.Commands where

import Control.Monad.IO.Class
import Crypto.PasswordStore (pbkdf2, genSaltIO, makePasswordSaltWith)
import Data.ByteString (ByteString)
import Data.Pool
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (encodeUtf8)
import Database.Beam
import Database.Beam.Backend.SQL.BeamExtensions
import Database.Beam.Postgres
import Gargoyle.PostgreSQL.Connect (withDb)
import System.Environment (getArgs)

import Backend.Schema
import Common.Schema

makePasswordHash
  :: MonadIO m
  => Text
  -> m ByteString
makePasswordHash pw = do
  salt <- liftIO genSaltIO
  return $ makePasswordSaltWith pbkdf2 (2^) (encodeUtf8 pw) salt 14

createAccount :: IO ()
createAccount = do
  xs <- getArgs
  case xs of
    [email,pw] -> do
      hash <- makePasswordHash (T.pack pw)
      _ <- withDb "db" $ \pool -> withResource pool $ \conn -> runBeamPostgres conn $
        runInsertReturningList $ insert (_db_accounts db) $ insertExpressions
          [ Account
              { _account_id = default_
              , _account_email = val_ $ T.pack email
              , _account_passwordHash = val_ $ Just hash
              , _account_passwordResetNonce = val_ $ Nothing
              }
          ]
      putStrLn "Added user."
    _ -> putStrLn "Usage: create-password EMAIL PASSWORD"
