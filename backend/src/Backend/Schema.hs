{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeFamilies #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Backend.Schema where

import Data.Time
import Database.Beam.AutoMigrate
import Database.Beam.Backend
import Database.Beam.Postgres
import Database.Beam.Query
import Database.Beam.Schema.Tables

import Common.Schema

utctime :: BeamSqlBackend be => DataType be UTCTime
utctime = DataType (timestampType Nothing True)

db :: DatabaseSettings Postgres Db
db = defaultDbSettings

dbAnn :: AnnotatedDatabaseSettings Postgres Db
dbAnn = defaultAnnotatedDbSettings db

runMigrations :: Connection -> IO ()
runMigrations conn = do
  tryRunMigrationsWithEditUpdate dbAnn conn
